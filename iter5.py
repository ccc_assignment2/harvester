# -*- coding: utf-8 -*-
"""
Created on Fri May  3 18:14:11 2019

@author: Spoorthi
"""

import os
import json
import pandas as pd
import re
import numpy as np
from pandas.io.json import json_normalize
import itertools
import emoji
import nltk
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize
from nltk.tokenize import WhitespaceTokenizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics


#Setting wd 
os.chdir("D:\\Cluster and Cloud Computing\\Assignment\\Assignment 2\\")
with open('D:\\Cluster and Cloud Computing\\Assignment\\Assignment 2\\2014-1-tweets.json',encoding="utf8") as json_file:  
    data = json.load(json_file)
    
#Normalising rows
df=pd.DataFrame(json_normalize(data["rows"]))
df.rename(columns={'doc.lang':'lang'},
                 inplace=True)
#Use necessary columns only
subset_data=pd.DataFrame()
lang_subset=df[(df.lang=="en")]
subset_data = lang_subset[['id','doc.text']].copy()

#df.drop_duplicates(subset=['tidy_tweets'], keep=False)
#Rename columns
subset_data.rename(columns={'doc.text':'text'},
                 inplace=True)

#Eliminate URLs
subset_data['cleaned']=subset_data['text'].apply(lambda x: re.sub(r"http\S+","",x)) 

#Function to eliminate mentions and convert tweets to lowercase
def removeMentionsCase(input_txt, pattern):
    r = re.findall(pattern, input_txt)
    for i in r:
        input_txt = re.sub(i, '', input_txt)        
    input_txt=input_txt.lower()       
    return input_txt 

subset_data['tidy_tweet'] = np.vectorize(removeMentionsCase)(subset_data["cleaned"], "@[\w]*")

#Contraction expand 
#correct spelling (replace more than 2 repetitions of chars with max 2 char occurence) 
#smiley and emoticon treatment
def expandContraction(input_txt):
    tweet = input_txt.replace("’","'")
    words = tweet.split()
    contracted = [CONTRACTION[word] if word in CONTRACTION else word for word in words]
    cleaned = [emotion[word] if word in emotion else word for word in contracted]
    tweet = " ".join(cleaned)
    tweet = ''.join(''.join(s)[:2] for _, s in itertools.groupby(tweet))
    tweet = emoji.demojize(tweet)
    tweet = tweet.replace(":"," ")
    tweet = ' '.join(tweet.split())
    return tweet

subset_data['contraction'] = np.vectorize(expandContraction)(subset_data["tidy_tweet"])

#keep only necessary values in tweets
subset_data['clean_tweet'] = subset_data['contraction'].str.replace("[^0-9a-zA-Z#]", " ")
           
#Set stopwords
stop_words = set(stopwords.words('english'))

#Tokenize and eliminate stopwords
subset_data['tokenised']=subset_data['clean_tweet'].apply(lambda x: WhitespaceTokenizer().tokenize(x)) 
subset_data['tokenised']=subset_data['tokenised'].apply(lambda x: ' '.join([w for w in x if not w in stop_words]))

#Classification 

#Import train data
train_data=pd.read_csv('emotional.csv')
#Tranform text to feauture vector
tf=TfidfVectorizer()
#Initial fitting of parameters
text_tf= tf.fit_transform(train_data['text'])
#Split training data as train and test
X_train, X_test, y_train, y_test = train_test_split(
    text_tf, train_data['emotion'], test_size=0.3, random_state=123)

#Fit the model
clf = MultinomialNB().fit(X_train, y_train)
#Predict for test 
predicted= clf.predict(X_test)
#Check accuracy
print("MultinomialNB Accuracy:",metrics.accuracy_score(y_test, predicted))
#Transform tweets 
transformed_data = tf.transform(subset_data['tokenised'])
#Predict for tweets
predicted_values= clf.predict(transformed_data)
#Convert predicted val to series
predicted_series = pd.Series(predicted_values)
#Concat predicted val with tweets
subset_data=subset_data.reset_index(drop=True)
result=pd.concat([subset_data, predicted_series],axis=1)

#Duplicate tweets